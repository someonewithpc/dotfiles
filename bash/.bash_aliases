alias gdb="gdb -q"
alias ls='ls --color=auto'
alias xcopy='xclip -selection clipboard'
alias pikaur='pikaur --nodiff --noedit'
alias pypi2pkgbuild.py='PKGEXT=.pkg.tar pypi2pkgbuild.py -g cython -b /tmp/pypi2pkgbuild/ -f'
